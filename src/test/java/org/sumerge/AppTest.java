package org.sumerge;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/*
 * Unit test for simple App.
 */

public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    private static final Logger logger = LogManager.getLogger(App.class);

    @Test
    public void shouldAnswerWithTrue()
    {
        BasicConfigurator.configure();
        logger.info("Hello world Test with profile: " + System.getProperty("profileId"));
        assertTrue( true );
    }
}
